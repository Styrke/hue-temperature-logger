import axios from 'axios';
import config from './config';
import { appendFile, readFileSync, writeFile } from 'fs';
import * as moment from 'moment';

interface NetworkDevice {
    id: string;
    internalipaddress: string;
    is_hue_bridge?: boolean;
}

interface TemperatureReading {
    sensorName: string;
    temperature: number;
    lastUpdated: string;
}

/**
 * Check whether the device that has a given ip on the network is a Philips Hue bridge.
 * @param ip The ip of the device to be checked.
 * @returns A boolean indication of whether the device is a Hue bridge or not.
 */
async function ipIsHueBridge(ip: string): Promise<boolean> {
    try {
        const response = await axios.get('http://' + ip + '/api/test/config', { timeout: 1000 });
        return response.status == 200;
    }
    catch (e) {
        return false;
    }
}

/**
 * Use the Hue discovery service to find the ip of the local Hue Bridge
 */
async function discoverHueBridge(): Promise<string> {
    // Get list of potential devices
    const devices: Array<NetworkDevice> = (await axios.get('https://discovery.meethue.com/')).data
    
    // Find out whether each device is a hue bridge
    const deviceStatuses = await Promise.all(devices.map(device => ipIsHueBridge(device.internalipaddress)))

    // Add the hue bridge status to each device in the list
    const devicesWithStatus = devices.map((device, index) => {
        device.is_hue_bridge = deviceStatuses[index]
        return device
    })

    // Pick out a device that is actually a hue bridge
    for (let device of devicesWithStatus) {
        if (device.is_hue_bridge)
            return device.internalipaddress;
    }
}

const nextKey = (key) => (parseInt(key) + 1).toString()

/**
 * Get readings from all temperature sensors that are connected to the hue bridge with the given ip.
 * @param ip The ip of the hue bridge to read sensor temperatures from
 * @returns Array of temperature readings from connected sensors
 */
async function getTemperatureReadings(ip: string): Promise<Array<TemperatureReading>> {
    const sensors = (await axios.get('http://' + ip + '/api/' + config.hueUsername + '/sensors')).data;
    let result = [];
    for (let key in sensors) {
        let element = sensors[key];
        if (element.type != 'ZLLTemperature') continue;  // Skip everything that isn't a temperature sensor
        result.push({
            sensorName: sensors[nextKey(key)].name,  // The real named of each temperature sensor can be read only from the next sensor in the list
            temperature: element.state.temperature,
            lastUpdated: element.state.lastupdated,
        });
    }
    return result;
}

/**
 * Append an array of temperature readings to a log file
 * @param temperatureReadings Array of temperature readings to save
 */
function saveTemperatureReadings(temperatureReadings: Array<TemperatureReading>) {
    const saveObject = {
        datetime: new Date(),
        sensorReadings: temperatureReadings,
    }
    const filename = 'logs/' + moment().format('YYYY-MM') + '.log'
    appendFile(filename, JSON.stringify(saveObject) + '\n', (err) => {if (err) console.log('Error during logging: ', err)})
}

/**
 * Get the ip that the Hue Bridge had the last time it was discovered
 */
function getLastHueBridgeIp(): string {
    try {
        return readFileSync('hue_bridge_ip.txt', {encoding: 'utf8'})
    } catch (err) {
        return null
    }
}

async function main() {
    let hueBridgeIp = getLastHueBridgeIp()
    
    // If we haven't found the ip of the hue bridge earlier or the ip changed, we must discover the ip and save it for later
    if (!hueBridgeIp || !await ipIsHueBridge(hueBridgeIp)) {
        hueBridgeIp = await discoverHueBridge()
        writeFile('hue_bridge_ip.txt', hueBridgeIp, { encoding: 'utf8' }, err => { if (err) console.log(err) })
    }
    
    saveTemperatureReadings(await getTemperatureReadings(hueBridgeIp))
}

main()