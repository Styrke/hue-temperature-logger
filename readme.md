# Hue Temperature Logger

Philips Hue Motion Sensors also has temperature sensors built in, and their readings can be accessed through the Hue API.
I thought it would be fun to save the temperature readings regularly throughout the day and use it to plot the temperature in my home.
The goal with this project is to do the logging, and to practice using TypeScript etc.

## Setup

1. Get a username for your Hue Bridge as [described here](https://developers.meethue.com/documentation/getting-started). Make a copy of `config-template.ts` called `config.ts` and put your username in it.
2. `npm install`
3. `npm run build`
4. `npm run main`